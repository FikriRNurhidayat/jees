const express = require('express');
const app = express();
const exphbs = require('express-handlebars');
const port = 9000

app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');
app.use(express.static('public'))

app.get('/', (req, res) => {
  res.render('index');
})

app.listen(port, ()=> {
  console.log(`Server started at ${Date()}`);
  console.log(`Listening on port ${port}!`);
})
